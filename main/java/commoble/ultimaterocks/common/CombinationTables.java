package commoble.ultimaterocks.common;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import commoble.ultimaterocks.common.blocks.BlockOreVariant;
import commoble.ultimaterocks.common.blocks.BlockRedstoneVariant;
import commoble.ultimaterocks.common.blocks.BlockStairVariant;
import commoble.ultimaterocks.common.blocks.BlockStoneVariant;
import commoble.ultimaterocks.common.enums.EnumOreType;
import commoble.ultimaterocks.common.enums.EnumRockCategory;
import commoble.ultimaterocks.common.enums.EnumRockType;
import commoble.ultimaterocks.common.enums.EnumBlockType;
import commoble.ultimaterocks.common.enums.EnumSlabType;
import commoble.ultimaterocks.common.items.ItemBlockVariants;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStone;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.registries.IForgeRegistry;

public class CombinationTables
{
	public static final int ROCK_TYPE_COUNT = EnumRockType.values().length;
	public static final int ORE_TYPE_COUNT = EnumOreType.values().length;
	public static final int BLOCK_TYPE_COUNT = EnumBlockType.values().length;
	public static final int SLAB_TYPE_COUNT = EnumSlabType.values().length;

	/**
	 * Lookup table of vanilla blockstates -> ore id
	 */
	public static HashMap<IBlockState, Integer> oreSubstitutionTable;
	/**
	 * Array of all rocktype/blocktype combination identifiers, e.g. diorite_stone, gneiss_diamond, etc
	 */
	public static String[][] oreBlockNames = new String[ROCK_TYPE_COUNT][ORE_TYPE_COUNT];
	public static String[][] buildingBlockNames = new String[ROCK_TYPE_COUNT][ORE_TYPE_COUNT];
	
	/**
	 * Array of all rocktype/blocktype combination stone blocks, set by RegistryEventHandler during block registration
	 */
	public static Block[][] stoneBlockArray = new Block[ROCK_TYPE_COUNT][ORE_TYPE_COUNT];
	
	public static Block[][] buildingBlockArray = new Block[ROCK_TYPE_COUNT][BLOCK_TYPE_COUNT];
	
	/**
	 * Array of all stair blocks
	 */
	public static BlockStairVariant[] cobbleStairBlocks = new BlockStairVariant[ROCK_TYPE_COUNT];
	public static BlockStairVariant[] brickStairBlocks = new BlockStairVariant[ROCK_TYPE_COUNT];
	
	/**
	 * Array of all itemblocks for the stone and ore blocks
	 */
	public static ItemBlock[][] stoneItemBlockArray = new ItemBlock[ROCK_TYPE_COUNT][ORE_TYPE_COUNT];
	// and the building blocks
	public static ItemBlock[][] buildingBlockItemBlocks = new ItemBlock[ROCK_TYPE_COUNT][BLOCK_TYPE_COUNT];
	
	/**
	 * Array of all itemblocks for the stairs
	 */
	public static ItemBlock[] cobbleStairItemBlocks = new ItemBlock[ROCK_TYPE_COUNT];
	public static ItemBlock[] brickStairItemBlocks = new ItemBlock[ROCK_TYPE_COUNT];
	
	/**
	 * Lit redstone needs a fudge
	 */
	public static Block[] litRedstoneArray = new BlockRedstoneVariant[ROCK_TYPE_COUNT];
	/**
	 * Array of all rocktype/blocktype combination default blockstates, for optimized lookup
	 * These are to be set by RegistryEventHandler during block registration
	 */
	public static IBlockState[][] oreBlockStates = new IBlockState[ROCK_TYPE_COUNT][ORE_TYPE_COUNT];
	
	/**
	 * Four sets, each corresponding with one of the four rock classifications and containing each rock type that belongs there
	 */
	public static ArrayList<ArrayList<EnumRockType>> rocksByCategory = new ArrayList<ArrayList<EnumRockType>>(4);
	
	
	// set the names asap
	static
	{
		// also initialize the category tables
		for (int i=0; i<4; i++)
		{
			rocksByCategory.add(new ArrayList<EnumRockType>());
		}
		
		// now do the names and assign rocks to the category sets
		for (int i=0; i<ROCK_TYPE_COUNT; i++)
		{
			EnumRockType rock = EnumRockType.byRockID(i);
			EnumRockCategory cat = rock.getCategory();
			rocksByCategory.get(cat.ordinal()).add(rock);
			String rockString = EnumRockType.byRockID(i).getName();
			
			for (int j=0; j<ORE_TYPE_COUNT; j++)
			{
				oreBlockNames[i][j] = rockString + "_" + EnumOreType.byOreID(j).getName();
			}
			for (int j=0; j<BLOCK_TYPE_COUNT; j++)
			{
				buildingBlockNames[i][j] = rockString + "_" + EnumBlockType.byID(j).getName();
			}
		}
	}
	
	/**
	 * Creates the lookup table for vanilla block -> ore type
	 * This is done by blockstate
	 */
	public static void defineOreSubstitutionTable()
	{
		oreSubstitutionTable = new HashMap<IBlockState, Integer>();
		oreSubstitutionTable.put(Blocks.STONE.getStateFromMeta(BlockStone.EnumType.STONE.getMetadata()), EnumOreType.STONE.getOreID());
		oreSubstitutionTable.put(Blocks.STONE.getStateFromMeta(BlockStone.EnumType.GRANITE.getMetadata()), EnumOreType.STONE.getOreID());
		oreSubstitutionTable.put(Blocks.STONE.getStateFromMeta(BlockStone.EnumType.DIORITE.getMetadata()), EnumOreType.STONE.getOreID());
		oreSubstitutionTable.put(Blocks.STONE.getStateFromMeta(BlockStone.EnumType.ANDESITE.getMetadata()), EnumOreType.STONE.getOreID());
		oreSubstitutionTable.put(Blocks.COAL_ORE.getDefaultState(), EnumOreType.COAL.getOreID());
		oreSubstitutionTable.put(Blocks.IRON_ORE.getDefaultState(), EnumOreType.IRON.getOreID());
		oreSubstitutionTable.put(Blocks.REDSTONE_ORE.getDefaultState(), EnumOreType.REDSTONE.getOreID());
		oreSubstitutionTable.put(Blocks.GOLD_ORE.getDefaultState(), EnumOreType.GOLD.getOreID());
		oreSubstitutionTable.put(Blocks.DIAMOND_ORE.getDefaultState(), EnumOreType.DIAMOND.getOreID());
		oreSubstitutionTable.put(Blocks.LAPIS_ORE.getDefaultState(), EnumOreType.LAPIS.getOreID());
		oreSubstitutionTable.put(Blocks.EMERALD_ORE.getDefaultState(), EnumOreType.EMERALD.getOreID());
		
		
		// TODO if blocks from other mods are to be substituted, this is a good place to do it
	}
	
	/**
	 * To be called by the RegistryEventHandler
	 */
	public static void registerAllBlocks(IForgeRegistry<Block> registry)
	{
		for (int i=0; i<ROCK_TYPE_COUNT; i++)
		{
			// register the stone blocks and the ores
			stoneBlockArray[i][EnumOreType.STONE.getOreID()] =
					registerBlock(registry, new BlockStoneVariant(), oreBlockNames[i][EnumOreType.STONE.getOreID()]);
			stoneBlockArray[i][EnumOreType.COAL.getOreID()] =
					registerBlock(registry, new BlockOreVariant(Items.COAL,0,2), oreBlockNames[i][EnumOreType.COAL.getOreID()]);
			stoneBlockArray[i][EnumOreType.IRON.getOreID()] =
					registerBlock(registry, new BlockOreVariant(Item.getItemFromBlock(Blocks.IRON_ORE),0,0), oreBlockNames[i][EnumOreType.IRON.getOreID()]);
			stoneBlockArray[i][EnumOreType.REDSTONE.getOreID()] =
					registerBlock(registry, new BlockRedstoneVariant(false), oreBlockNames[i][EnumOreType.REDSTONE.getOreID()]);
			litRedstoneArray[i] =
					registerBlock(registry, new BlockRedstoneVariant(true), oreBlockNames[i][EnumOreType.REDSTONE.getOreID()]+"_lit");
			// map lit and unlit redstone to each other
			BlockRedstoneVariant.litToUnlitBlockMap.put(
					(BlockRedstoneVariant)litRedstoneArray[i],
					(BlockRedstoneVariant)stoneBlockArray[i][EnumOreType.REDSTONE.getOreID()]);
			BlockRedstoneVariant.unlitToLitBlockMap.put(
					(BlockRedstoneVariant)stoneBlockArray[i][EnumOreType.REDSTONE.getOreID()], 
					(BlockRedstoneVariant)litRedstoneArray[i]);
			stoneBlockArray[i][EnumOreType.GOLD.getOreID()] =
					registerBlock(registry, new BlockOreVariant(Item.getItemFromBlock(Blocks.GOLD_ORE),0,0), oreBlockNames[i][EnumOreType.GOLD.getOreID()]);
			stoneBlockArray[i][EnumOreType.DIAMOND.getOreID()] =
					registerBlock(registry, new BlockOreVariant(Items.DIAMOND,3,7), oreBlockNames[i][EnumOreType.DIAMOND.getOreID()]);
			stoneBlockArray[i][EnumOreType.LAPIS.getOreID()] =
					registerBlock(registry, new BlockOreVariant(Items.DYE,2,5), oreBlockNames[i][EnumOreType.LAPIS.getOreID()]);
			stoneBlockArray[i][EnumOreType.EMERALD.getOreID()] =
					registerBlock(registry, new BlockOreVariant(Items.EMERALD,3,7), oreBlockNames[i][EnumOreType.EMERALD.getOreID()]);
			
			for (int j=0; j<ORE_TYPE_COUNT; j++)
			{
				oreBlockStates[i][j] = stoneBlockArray[i][j].getDefaultState();
			}
			
			// register building block variants
			for (int j=0; j<BLOCK_TYPE_COUNT; j++)
			{
				Block newBlock = new Block(Material.ROCK).setHardness(2.0F).setResistance(10.0F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
				String block_name = EnumRockType.byRockID(i).getName() + "_" + EnumBlockType.byID(j).getName();
				buildingBlockArray[i][j] = registerBlock(registry, newBlock, block_name);
			}
			
			//register stairs

			String cobble_stair_name = EnumRockType.byRockID(i).getName() + "_cobblestone_" + "stairs";
			cobbleStairBlocks[i] =
					registerBlock(registry, (new BlockStairVariant(getStoneBlockForRock(i).getDefaultState())), cobble_stair_name);

			String brick_stair_name = EnumRockType.byRockID(i).getName() + "_stone_brick_" + "stairs";
			brickStairBlocks[i] =
					registerBlock(registry, (new BlockStairVariant(getStoneBlockForRock(i).getDefaultState())), brick_stair_name);
					
		}
	}
	
	public static Block getStoneBlockForRock(EnumRockType rockType)
	{
		return getStoneBlockForRock(rockType.getRockID());
	}
	
	public static Block getStoneBlockForRock(int rockID)
	{
		return stoneBlockArray[rockID][0];	// id 0 is the stone block
	}
	
	public static void registerAllItems(IForgeRegistry<Item> registry)
	{
		for (int i=0; i<ROCK_TYPE_COUNT; i++)
		{
			registerItemBlockForCombo(registry, i, EnumOreType.STONE.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.COAL.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.IRON.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.REDSTONE.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.GOLD.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.DIAMOND.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.LAPIS.getOreID());
			registerItemBlockForCombo(registry, i, EnumOreType.EMERALD.getOreID());
			
			// item blocks for building blocks
			for (int j=0; j<BLOCK_TYPE_COUNT; j++)
			{
				buildingBlockItemBlocks[i][j] = registerItemBlock(registry, new ItemBlockVariants(buildingBlockArray[i][j]), buildingBlockNames[i][j]);
			}
			
			// stairs
			String cobble_stair_name = EnumRockType.byRockID(i).getName() + "_cobblestone_" + "stairs";
			cobbleStairItemBlocks[i] =
					registerItemBlock(registry, new ItemBlockVariants(cobbleStairBlocks[i]), cobble_stair_name);
			String brick_stair_name = EnumRockType.byRockID(i).getName() + "_stone_brick_" + "stairs";
			brickStairItemBlocks[i] =
					registerItemBlock(registry, new ItemBlockVariants(brickStairBlocks[i]), brick_stair_name);
		}
	}
	
	// shortcut for registering itemblocks for ores
	private static void registerItemBlockForCombo(IForgeRegistry<Item> registry, int rockID, int oreID)
	{
		stoneItemBlockArray[rockID][oreID] = registerItemBlock(registry, new ItemBlockVariants(stoneBlockArray[rockID][oreID]), oreBlockNames[rockID][oreID]);
	}
	
	private static String appendPrefix(String unPrefixedString)
	{
		return "ultimaterocks:" + unPrefixedString;
	}
	
	private static <T extends Block> T registerBlock(IForgeRegistry<Block> registry, T newBlock, String name)
	{
		name = appendPrefix(name);
		newBlock.setUnlocalizedName(name);
		newBlock.setRegistryName(name);
		registry.register(newBlock);
		return newBlock;
	}
	
	private static <T extends Item> T registerItem(IForgeRegistry<Item> registry, T newItem, String name)
	{
		name = appendPrefix(name);
		newItem.setUnlocalizedName(name);
		newItem.setRegistryName(name);
		registry.register(newItem);
		return newItem;
	}
	
	private static <T extends ItemBlock> T registerItemBlock(IForgeRegistry<Item> registry, T newItemBlock, String name)
	{
		name = appendPrefix(name);
		newItemBlock.setUnlocalizedName(name);
		newItemBlock.setRegistryName(name);
		registry.register(newItemBlock);
		return newItemBlock;
	}
}
