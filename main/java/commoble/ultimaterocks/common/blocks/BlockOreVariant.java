package commoble.ultimaterocks.common.blocks;

import java.util.Random;

import javax.annotation.Nullable;

import commoble.ultimaterocks.common.enums.EnumRockType;
import net.minecraft.block.BlockOre;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class BlockOreVariant extends BlockOre
{
	public static String[] coal_name_list = new String[EnumRockType.values().length];
	public static String[] iron_name_list = new String[EnumRockType.values().length];
	public static String[] gold_name_list = new String[EnumRockType.values().length];
	public static String[] diamond_name_list = new String[EnumRockType.values().length];
	public static String[] lapis_name_list = new String[EnumRockType.values().length];
	public static String[] emerald_name_list = new String[EnumRockType.values().length];
	static
	{
		for (int i=0; i<EnumRockType.values().length; i++)
		{
			coal_name_list[i] = EnumRockType.byRockID(i).getName() + "_coal";
			iron_name_list[i] = EnumRockType.byRockID(i).getName() + "_iron";
			gold_name_list[i] = EnumRockType.byRockID(i).getName() + "_gold";
			diamond_name_list[i] = EnumRockType.byRockID(i).getName() + "_diamond";
			lapis_name_list[i] = EnumRockType.byRockID(i).getName() + "_lapis";
			emerald_name_list[i] = EnumRockType.byRockID(i).getName() + "_emerald";
		}
	}
	
	protected Item itemDropped;	// null if it drops an itemblock
	public int minXP;
	public int maxXP;
	
	/**
	 * Set itemDropped to NULL to drop an itemblock
	 */
    public BlockOreVariant(Item itemDropped, int minXP, int maxXP)
    {
        super(MapColor.STONE);
		this.setHardness(3.0F);
		this.setResistance(5.0F);
		this.setSoundType(SoundType.STONE);
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        this.itemDropped = itemDropped;
        this.minXP = minXP;
        this.maxXP = maxXP;
    }

    /**
     * Get the Item that this Block should drop when harvested.
     */
    @Nullable
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return this.itemDropped;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random random)
    {	// drop lots of lapis if it drops lapis, otherwise 1 item
        return (this.itemDropped == Items.DYE ? 4 + random.nextInt(5) : 1);
    }
    
    @Override
    public int getExpDrop(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune)
    {
        Random rand = world instanceof World ? ((World)world).rand : new Random();
        if (this.getItemDropped(state, rand, fortune) instanceof ItemBlock)
        {
        	return 0; // no exp if it drops a block
        }
        return MathHelper.getInt(rand, minXP, maxXP);
    }

    /**
     * Get the quantity dropped based on the given fortune level
     */
    public int quantityDroppedWithBonus(int fortune, Random random)
    {	// if it doesn't drop a block, have Fortune grant more items
        if (fortune > 0 && !(Item.getItemFromBlock(this) instanceof ItemBlock))
        {
            int i = random.nextInt(fortune + 2) - 1;

            if (i < 0)
            {
                i = 0;
            }

            return this.quantityDropped(random) * (i + 1);
        }
        else
        {
            return this.quantityDropped(random);
        }
    }

    /**
     * Gets the metadata of the item this Block can drop. This method is called when the block gets destroyed. It
     * returns the metadata of the dropped item based on the old metadata of the block.
     */
    public int damageDropped(IBlockState state)
    {
    	return this.itemDropped == Items.DYE ? EnumDyeColor.BLUE.getDyeDamage() : 0;
    }
}
