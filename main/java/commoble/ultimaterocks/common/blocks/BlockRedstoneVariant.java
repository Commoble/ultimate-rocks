package commoble.ultimaterocks.common.blocks;

import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import commoble.ultimaterocks.common.CommonProxy;
import commoble.ultimaterocks.common.enums.EnumRockType;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRedstoneOre;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockRedstoneVariant extends Block 
// I'd like to extend vanilla redstone ore but it does some annoying things with private functions
// so I'll just copy everything from redstone and edit where needed
{
	public static String[] name_list = new String[EnumRockType.values().length];
	
	// these mappings are used to easily convert unlit blocks to the correct lit blocks and vice-versa
	// the mappings are set when the blocks are registered in RegistryEventManager
	// under certain assumptions, it might be possible to infer the blockID of one from the other without these
	// but that feels hacky and I'm not confident about those assumptions
	/**
	 * Input Key: unlit redstone ore variant
	 * Output Value: corresponding lit redstone ore variant
	 */
	public static Hashtable<BlockRedstoneVariant,BlockRedstoneVariant> unlitToLitBlockMap = new Hashtable<BlockRedstoneVariant,BlockRedstoneVariant>();

	/**
	 * Input Key: lit redstone ore variant
	 * Output Value: corresponding unlit redstone ore variant
	 */
	public static Hashtable<BlockRedstoneVariant,BlockRedstoneVariant> litToUnlitBlockMap = new Hashtable<BlockRedstoneVariant,BlockRedstoneVariant>();
	static
	{
		for (int i=0; i<EnumRockType.values().length; i++)
		{
			name_list[i] = EnumRockType.byRockID(i).getName() + "_redstone";
		}
	}
	
    protected final boolean isOn;
	
	public BlockRedstoneVariant(boolean isOn)
	{
        super(Material.ROCK);

        if (isOn)
        {
            this.setTickRandomly(true);
        }

        this.isOn = isOn;
		this.setHardness(3.0F);
		this.setResistance(5.0F);
		this.setSoundType(SoundType.STONE);
		if (isOn)
		{
			this.setLightLevel(0.625F);
		}
		else
		{
			this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		}
	}
	
	/**
	 * If this block is lit, get the unlit block variant
	 * If this block is unlit, get the lit block variant
	 * Variant-to-variant mappings are set by RegistryEventManager when the blocks are registered
	 */
	public BlockRedstoneVariant getAlternateLightingState()
	{
		if (this.isOn)
		{
			return this.litToUnlitBlockMap.get(this);
		}
		else
		{
			return this.unlitToLitBlockMap.get(this);
		}
	}
	
	public MapColor getMapColor(IBlockState state)
	{
		return MapColor.STONE;
	}

    /**
     * How many world ticks before ticking
     */
    public int tickRate(World worldIn)
    {
        return 30;
    }

    public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn)
    {
        this.activate(worldIn, pos);
        super.onBlockClicked(worldIn, pos, playerIn);
    }

    /**
     * Triggered whenever an entity collides with this block (enters into the block)
     */
    public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn)
    {
        this.activate(worldIn, pos);
        super.onEntityWalk(worldIn, pos, entityIn);
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        this.activate(worldIn, pos);
        return super.onBlockActivated(worldIn, pos, state, playerIn, hand, side, hitX, hitY, hitZ);
    }

    protected void activate(World worldIn, BlockPos pos)
    {
    	// when activated, make some particles, then turn it on if it's not on
        this.spawnParticles(worldIn, pos);

        if (!this.isOn)
        {
            worldIn.setBlockState(pos, this.getAlternateLightingState().getDefaultState());
        }
    }

    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand)
    {	// when block ticked, if on, turn off
        if (this.isOn)
        {
            worldIn.setBlockState(pos, this.getAlternateLightingState().getDefaultState());
        }
    }

    /**
     * Get the Item that this Block should drop when harvested.
     */
    @Nullable
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Items.REDSTONE;
    }

    /**
     * Get the quantity dropped based on the given fortune level
     */
    public int quantityDroppedWithBonus(int fortune, Random random)
    {
        return this.quantityDropped(random) + random.nextInt(fortune + 1);
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int quantityDropped(Random random)
    {
        return 4 + random.nextInt(2);
    }

    /**
     * Spawns this Block's drops into the World as EntityItems.
     */
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
    {
        super.dropBlockAsItemWithChance(worldIn, pos, state, chance, fortune);
    }

    @Override
    public int getExpDrop(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune)
    {
        if (this.getItemDropped(state, RANDOM, fortune) != Item.getItemFromBlock(this))
        {
            return 1 + RANDOM.nextInt(5);
        }
        return 0;
    }

    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
    {
        if (this.isOn)
        {
            this.spawnParticles(worldIn, pos);
        }
    }

    private void spawnParticles(World worldIn, BlockPos pos)
    {
        Random random = worldIn.rand;
        double d0 = 0.0625D;

        for (int i = 0; i < 6; ++i)
        {
            double d1 = (double)((float)pos.getX() + random.nextFloat());
            double d2 = (double)((float)pos.getY() + random.nextFloat());
            double d3 = (double)((float)pos.getZ() + random.nextFloat());

            if (i == 0 && !worldIn.getBlockState(pos.up()).isOpaqueCube())
            {
                d2 = (double)pos.getY() + 0.0625D + 1.0D;
            }

            if (i == 1 && !worldIn.getBlockState(pos.down()).isOpaqueCube())
            {
                d2 = (double)pos.getY() - 0.0625D;
            }

            if (i == 2 && !worldIn.getBlockState(pos.south()).isOpaqueCube())
            {
                d3 = (double)pos.getZ() + 0.0625D + 1.0D;
            }

            if (i == 3 && !worldIn.getBlockState(pos.north()).isOpaqueCube())
            {
                d3 = (double)pos.getZ() - 0.0625D;
            }

            if (i == 4 && !worldIn.getBlockState(pos.east()).isOpaqueCube())
            {
                d1 = (double)pos.getX() + 0.0625D + 1.0D;
            }

            if (i == 5 && !worldIn.getBlockState(pos.west()).isOpaqueCube())
            {
                d1 = (double)pos.getX() - 0.0625D;
            }

            if (d1 < (double)pos.getX() || d1 > (double)(pos.getX() + 1) || d2 < 0.0D || d2 > (double)(pos.getY() + 1) || d3 < (double)pos.getZ() || d3 > (double)(pos.getZ() + 1))
            {
                worldIn.spawnParticle(EnumParticleTypes.REDSTONE, d1, d2, d3, 0.0D, 0.0D, 0.0D, new int[0]);
            }
        }
    }

    protected ItemStack createStackedBlock(IBlockState state)
    {
    	if (this.isOn)
    	{
    		return new ItemStack(this.getAlternateLightingState());
    	}
        return new ItemStack(this);
    }

    @Nullable
    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
    {
        return new ItemStack(Item.getItemFromBlock(Blocks.REDSTONE_ORE), 1, this.damageDropped(state));
    }
}
