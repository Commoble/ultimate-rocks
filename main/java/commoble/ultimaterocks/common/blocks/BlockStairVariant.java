package commoble.ultimaterocks.common.blocks;

import net.minecraft.block.BlockStairs;
import net.minecraft.block.state.IBlockState;

/**
 * Need to make a subclass because the BlockStairs constructor is protected
 *
 */
public class BlockStairVariant extends BlockStairs
{
	public BlockStairVariant(IBlockState baseBlock)
	{
		super(baseBlock);
	}
}
