package commoble.ultimaterocks.common;

import commoble.ultimaterocks.common.blocks.BlockOreVariant;
import commoble.ultimaterocks.common.blocks.BlockRedstoneVariant;
import commoble.ultimaterocks.common.blocks.BlockStoneVariant;
import commoble.ultimaterocks.common.items.ItemBlockVariants;
import commoble.ultimaterocks.common.world.WorldGenManager;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy
{
	// config values
	// TODO config ideas:
	// ore replacement off/on
	
	// plutonic rocks
	//public static BlockRockVariants blockDiorite;
	//public static BlockRockVariants blockGabbro;
	//public static BlockRocks blockGranite;
	
	// volcanic rocks
	//public static BlockRocks blockAndesite;
	//public static BlockRocks blockBasalt;
	//public static BlockRockVariants blockRhyolite;
	
	// metamorphic rocks
	//public static BlockRockVariants blockGneiss;
	//public static BlockRocks blockMarble;
	//public static BlockRocks blockPhyllite;
	//public static BlockRocks blockSchist;
	//public static BlockRocks blockSlate;
	
	// sedimentary rocks
	//public static BlockRocks blockChalk;
	//public static BlockRocks blockChert;
	//public static BlockRocks blockConglomerate;
	//public static BlockRocks blockDolomite;
	//public static BlockRockVariants blockLimestone;
	//public static BlockRocks blockShale;
	
	// ore variants
	//public static Block[][] blockVariants = new Block[ROCK_TYPE_COUNT][ORE_TYPE_COUNT];
	/*public static BlockStoneVariant[] blockStoneVariants = new BlockStoneVariant[ROCK_TYPE_COUNT];
	public static BlockRedstoneVariant[] blockRedstoneVariants = new BlockRedstoneVariant[ROCK_TYPE_COUNT];
	public static BlockRedstoneVariant[] blockRedstoneVariants_lit = new BlockRedstoneVariant[ROCK_TYPE_COUNT];
	public static BlockOreVariant[] blockCoalVariants = new BlockOreVariant[ROCK_TYPE_COUNT];
	public static BlockOreVariant[] blockIronVariants = new BlockOreVariant[ROCK_TYPE_COUNT];
	public static BlockOreVariant[] blockGoldVariants = new BlockOreVariant[ROCK_TYPE_COUNT];
	public static BlockOreVariant[] blockDiamondVariants = new BlockOreVariant[ROCK_TYPE_COUNT];
	public static BlockOreVariant[] blockLapisVariants = new BlockOreVariant[ROCK_TYPE_COUNT];
	public static BlockOreVariant[] blockEmeraldVariants = new BlockOreVariant[ROCK_TYPE_COUNT];*/
	
	// items
	
	// itemblocks
	/*public static ItemBlockVariants[] itemBlockStoneVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockRedstoneVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockCoalVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockIronVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockGoldVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockDiamondVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockLapisVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];
	public static ItemBlockVariants[] itemBlockEmeraldVariants = new ItemBlockVariants[ROCK_TYPE_COUNT];*/
	
	// sounds
	
	// misc
	public static WorldGenManager worldGenManager = new WorldGenManager();
	
	public static int modEntityID = 0;
	
	/**
	 * Run before anything else;
	 * Declare configuration parameters;
	 * Register everything that doesn't require something else to be registered first
	 */
	public void preInit(FMLPreInitializationEvent event)
	{
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		Property prop;
		config.load();
		
		// TODO setup config file here
		
		config.save();
		
		// Registration
		// Blocks, Enchantments, Items, Potions, SoundEvents, and Biomes should be registered with registry events
		// Entities, Tile Entities, and Dimensions need to be registered here
		//this.registerTileEntities();
		//this.registerEntities();
		//this.registerPlanes();
		GameRegistry.registerWorldGenerator(worldGenManager, 10000);
	}
	
	/**
	 * The most important things to do in the main load event are:
	 * Register recipes, send FMLInterModComms messages to other mods, build data structures that shouldn't be in the other events
	 */
	public void load(FMLInitializationEvent event)
	{
		this.registerRecipes();
	}
	
	/**
	 * Handle interactions with other mods and complete setup
	 * e.g. registering creature spawning should go here due to other mods potentially creating new biomes
	 */
	public void postInit(FMLPostInitializationEvent event)
	{
		CombinationTables.defineOreSubstitutionTable();
	}
	
	private void registerRecipes()
	{

	}
}
