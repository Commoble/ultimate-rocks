package commoble.ultimaterocks.common.world;

import java.util.ArrayList;
import java.util.Random;

import commoble.ultimaterocks.common.CombinationTables;
import commoble.ultimaterocks.common.enums.EnumRockCategory;
import commoble.ultimaterocks.common.enums.EnumRockType;
import net.minecraftforge.event.world.WorldEvent;

/**
 * Tables of geological layers
 */
public class GeoTables
{
	// rock layers
	public static GeoRegion[] geoRegions = new GeoRegion[100];
	
	public static void generateTables(WorldEvent.CreateSpawnPosition event)
	{
		Random rand = new Random(event.getWorld().getSeed() + 12345l);
		for (int i=0; i<100; i++)
		{
			// decide on a rock category
			// for simplicity's sake, in the current generation system, all of minecraft is considered to be "near the surface"
			// on stupidworld, about 75% of surface rock is sedimentary rock
			EnumRockCategory cat;
			double layerweight;
			int weightFactorID = rand.nextInt(3);	// 0, 1, 2
			int catpicker = rand.nextInt(12);
			if (catpicker < 9)	// 75%
			{
				cat = EnumRockCategory.SEDIMENTARY;
				layerweight = 3 + rand.nextDouble()*7D;	// 3 to 10
			}
			else if (catpicker < 10)	// 25%
			{
				cat = EnumRockCategory.METAMORPHIC;
				layerweight = 10 + rand.nextDouble()*5D;	// 10 to 15
			}
			else if (catpicker < 11)	// 25%
			{
				cat = EnumRockCategory.PLUTONIC;
				layerweight = 10 + rand.nextDouble()*5D;	// 10 to 15
			}
			else	// 25%
			{
				cat = EnumRockCategory.VOLCANIC;
				layerweight = 5 + rand.nextDouble()*10D;	// 5 to 15
			}
			ArrayList<EnumRockType> rocklist = CombinationTables.rocksByCategory.get(cat.ordinal());
			EnumRockType rock = rocklist.get(rand.nextInt(rocklist.size()));
			geoRegions[i] = new GeoRegion(rock, layerweight, weightFactorID);
		}
	}
}
