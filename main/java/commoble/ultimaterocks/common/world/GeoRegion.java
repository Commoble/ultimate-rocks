package commoble.ultimaterocks.common.world;

import commoble.ultimaterocks.common.enums.EnumRockType;

public class GeoRegion
{
	public EnumRockType rockType;
	public double layerWeight;	// default layer weight
	public int weightFactorID;	// which of three factors used by the world generator to use (0, 1, or 2)
	
	public GeoRegion(EnumRockType rockType, double layerWeight, int weightFactorID)
	{
		this.rockType = rockType;
		this.layerWeight = layerWeight;
		this.weightFactorID = weightFactorID;
	}
}
