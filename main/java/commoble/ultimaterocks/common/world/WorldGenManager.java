package commoble.ultimaterocks.common.world;

import java.util.Random;

import commoble.ultimaterocks.common.CombinationTables;
import commoble.ultimaterocks.common.CommonProxy;
import commoble.ultimaterocks.common.enums.EnumRockCategory;
import commoble.ultimaterocks.common.enums.EnumRockType;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.NoiseGeneratorOctaves;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import scala.collection.concurrent.Debug;

public class WorldGenManager implements IWorldGenerator
{
	/**
	 * When the world is loaded on the server side, generate perlin noise tables
	 * Called when the WorldEventHandler receives a world-load event on the server side
	 */
	private double[] geoTableA;	//these are used for making big blobs of matching layers
	private double[] geoTableB;
	private double[] geoTableC;
	private double[] geoTableBroad;

	private NoiseGeneratorOctaves geoNoiseA;
	private NoiseGeneratorOctaves geoNoiseB;
	private NoiseGeneratorOctaves geoNoiseC;
	private NoiseGeneratorOctaves geoNoiseBroad;
	
	private static final double ONE_64TH = 0.015625;
	private static final double ONE_32TH = 0.03125D;
	private static final double ONE_16TH = 0.0625D;
	private static final double ONE_8TH = 0.125D;
	
	private Random rand = new Random();	// use responsibly! ALWAYS reset the seed before using!
	
	
	public void genNoiseTables(WorldEvent.CreateSpawnPosition event)
	{
		if (this.geoNoiseA != null)
			return;
		World world = event.getWorld();
		rand.setSeed(world.getSeed());	// this random needs to be the same whenever the world is loaded
		geoNoiseA = new NoiseGeneratorOctaves(rand,1);
		geoNoiseB = new NoiseGeneratorOctaves(rand,1);
		geoNoiseC = new NoiseGeneratorOctaves(rand,1);
		geoNoiseBroad = new NoiseGeneratorOctaves(rand,4);
		// 4 octaves: about -7 to 7, usually not higher than 5 or 6
		// 2 octaves: about -1.5 to 1.5, very rarely higher
		// 1 octave: about -0.5 to 0.5
	}
	
	/**
	 * Called by the game registry during world generation
	 */
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world,
			IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
	{
		if (world.isRemote)
			return;
		
		// check which plane we're generating in;
		// -1 = nether, 0 = overworld, 1 = END, other ID = some mod
		switch(world.provider.getDimension())
		{
			case -1:
				this.generateNether(world, random, chunkX, chunkZ);
			case 0:
				this.generateSurface(world, random, chunkX, chunkZ);
			case 1:
				this.generateEnd(world, random, chunkX, chunkZ);
		}
	}
	
	/**
	 * Handle Nether generation
	 * x,z = global tilespace coordinates, will be 0,0 in the local tilespace for the chunk
	 */
	private void generateNether(World world, Random random, int x, int z)
	{
		
	}
	
	/**
	 * Handle Surface generation
	 * x,z = chunk coordinates
	 */
	private void generateSurface(World world, Random random, int chunkX, int chunkZ)
	{
		BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos(0,0,0);
		Chunk chunk = world.getChunkFromChunkCoords(chunkX, chunkZ);
		
		int globalX = chunkX << 4;
		int globalZ = chunkZ << 4;
		//System.out.println(testRegion[0] + " and " + testRegion2[0]);
		
		// divide generation of each chunk into 64-tile-high subchunks
		// start generating from the bottom
		// TODO look into checking if reading directly from the chunk's storage data is more efficient
		
		//int subChunkCount = chunk.getTopFilledSegment() / 4;
		//for (int subchunk = 0; subchunk<subChunkCount; subchunk++)
		{
			// generate noise tables with an 8-tile buffer around the actual tilespace in each horizontal direction
			// this is because of the x/y/z offset noise
			int xOffset = chunkX*16;
			int zOffset = chunkZ*16;
			int size = 16;	// 16x16 chunk grid with 8-tile buffer on each side

			this.geoTableA = geoNoiseA.generateNoiseOctaves(geoTableA, xOffset, zOffset, size, size, ONE_32TH, ONE_32TH, 0);
			this.geoTableB = geoNoiseB.generateNoiseOctaves(geoTableB, xOffset, zOffset, size, size, ONE_32TH, ONE_32TH, 0);
			this.geoTableC = geoNoiseC.generateNoiseOctaves(geoTableC, xOffset, zOffset, size, size, ONE_32TH, ONE_32TH, 0);
			this.geoTableBroad = geoNoiseBroad.generateNoiseOctaves(geoTableBroad, xOffset, zOffset, size, size, 0.001D, 0.001D, 0);
			
			for (int i=0; i<16; i++)
			{
				for (int j=0; j<16; j++)
				{
					
					
					// getHeightValue uses local chunk tilespace
					int maxY = chunk.getHeightValue(i, j);
					int regionIndex = (i<<4)+j;
					
					// local variations
					double geoA = (geoTableA[regionIndex] + 1.5);	if (geoA < 0D) geoA = 0D;
					double geoB = (geoTableB[regionIndex] + 1.5);	if (geoB < 0D) geoA = 0D;
					double geoC = (geoTableC[regionIndex] + 1.5);	if (geoC < 0D) geoA = 0D;
					double geoBroad = (geoTableBroad[regionIndex] + 20) * 10;	// long-distance variation
					double weightFactorA = geoA-1D; if (weightFactorA < 0.1D) weightFactorA = 0.1D;
					double weightFactorB = geoB-1D; if (weightFactorB < 0.1D) weightFactorB = 0.1D;
					double weightFactorC = geoC-1D; if (weightFactorC < 0.1D) weightFactorC = 0.1D;
					double[] weightFactors = {weightFactorA*weightFactorB, weightFactorA*weightFactorC, weightFactorB*weightFactorC};
					
					//double geoHash = (geoA) * (geoB) * (geoC) * 12;
					double geoHash = (geoA + geoB + geoC) + geoBroad;
					// ID of the main geological region	(not counting sedimentary rock)
						// this is going to be roughly in the range (10,140)
						// we need to be in the range(0,100) but we can always mod by 100 later
					
					// get more layers above and below the primary one
					double upper_geoHash = geoHash + (geoA * 2);
					double lower_geoHash = geoHash - (geoB * 2);
					// fractional size multiplier of the top and bottom layers
					double top_layer_multiplier = upper_geoHash - Math.floor(upper_geoHash);
					double bottom_layer_multiplier = Math.ceil(lower_geoHash) - lower_geoHash;
					// list of layers in this column includes the top and bottom id and all integers between them
					int top_id = (int) (upper_geoHash) + 1;
					int bottom_id = (int)lower_geoHash;
					int layercount = top_id - bottom_id + 1;

					double[] weights = new double[layercount];
					double totalweight = 0;
					
					// calculate layer weights
					for (int layerindex = 0; layerindex < layercount; layerindex++)
					{
						int id = layerindex + bottom_id;
						GeoRegion region = GeoTables.geoRegions[id % 100];
						weights[layerindex] = region.layerWeight * weightFactors[region.weightFactorID];
						if (layerindex == 0) weights[layerindex] *= bottom_layer_multiplier;
						if (layerindex == layercount-1) weights[layerindex] *= top_layer_multiplier;
						totalweight += weights[layerindex];
					}
					
					// determine true height of each layer and get the data for the column
					LayerData[] layers = new LayerData[layercount];
					
					// do the first layer first
					double reciprocal_totalweight = 1 / totalweight;	// we could be using this a few times, faster to multiply
					int height = (int)(weights[0] * reciprocal_totalweight * maxY);
					layers[0] = new LayerData(height, GeoTables.geoRegions[bottom_id % 100].rockType);
					
					// use the weights and the height of this column in the chunk to determine the height of each layer's top
					for (int layerindex = 1; layerindex< layercount; layerindex++)
					{
						height = (int)(weights[layerindex] * reciprocal_totalweight * maxY) + layers[layerindex-1].topLevel;
						layers[layerindex] = new LayerData(height, GeoTables.geoRegions[(layerindex + bottom_id) % 100].rockType);
					}
					
					int currentLayer = 0;
					int topLayer = layercount-1;
					
					for (int k=0; k<maxY; k++)
					{
						pos.setPos(globalX|i, k, globalZ|j);
						if (layers[currentLayer].topLevel < k && currentLayer != topLayer) // safety check to prevent out-of-boundness
						{
							currentLayer++;
						}
						this.substituteBlockAt(chunk, pos, layers[currentLayer]);//x*16+z in local chunk tilespace
					}
					
					/*double pick1 = testRegion[j+i*16]*5D;
					pick1 = pick1*pick1;
					double pick2 = testRegion2[j+i*16]*3D;
					if (pick2 > pick1)
					{
						//System.out.println("So " + pick2 + " is higher than " + pick1 + " at " + (new BlockPos(globalX|i, 0, globalZ|j).toString()));
						int pick1i = (int)Math.floor(pick1)+100;
						int pick2i = (int)Math.floor(pick2)+100;
						for (int k=pick1i; k<pick2i; k++)
						{
							pos.setPos(globalX|i, k, globalZ|j);
							chunk.setBlockState(pos, Blocks.GRASS.getDefaultState());
						}
					}*/
					/*for (int k=0; k<maxY; k++)
					{
						pos.setPos(globalX|i, k, globalZ|j);
						this.substituteBlockAt(chunk, pos);
					}*/
				}
			}
		}
		
		chunk.setModified(true);
	}
	
	protected void substituteBlockAt(Chunk chunk, BlockPos pos, LayerData layer)
	{
		// cut out a cross-section of the world for visual debug
		/*if (pos.getX() >= 0 && pos.getX() < 16)
		{
			chunk.setBlockState(pos, Blocks.AIR.getDefaultState());
			return;
		}*/
		IBlockState oldState = chunk.getBlockState(pos);
		int oreID = getSubstitutionOreType(oldState);	// returns -1 if substitution should not be done
		if (oreID >= 0)
		{
			int rockID = layer.rocktype.getRockID();//getSubstitutionRockType(chunk, pos, layer);
			chunk.setBlockState(pos, CombinationTables.oreBlockStates[rockID][oreID]);
		}
	}
	
	/**
	 * Get the rock type to substitute for a given existing block position (always returns a valid rock type)
	 */
	protected int getSubstitutionRockType(Chunk chunk, BlockPos pos, LayerData layer)
	{
		//double sedDepthWeight = sedDepthRegion[regionIndex];
		return 0;
	}
	
	/**
	 * Get the ore type to substitute for a given existing state, returns -1 if it should not be substituted
	 */
	protected int getSubstitutionOreType(IBlockState state)
	{
		Integer oreID = CombinationTables.oreSubstitutionTable.get(state);	// returns null if it doesn't exist
		return (oreID == null ? -1 : oreID);
	}
	
	
	/**
	 * Handle End generation
	 * x,z = global tilespace coordinates, will be 0,0 in the local tilespace for the chunk
	 */
	private void generateEnd(World world, Random random, int x, int z)
	{
		
	}
	
	
	private void addOreSingle(Block block, World world, Random random, int x, int z, int xMax, int zMax, int chancesToSpawn, int yMin, int yMax)
	{
		assert yMax > yMin : "addOreSingle: The Maximum Y must be greater than the minimum Y";
		assert yMin > 0 : "addOreSingle: The minimum Y must be greater than 0";
		assert xMax > 0 && xMax <= 16 : "addOreSingle: The Maximum X must be greater than 0 and no more than 16";
		assert zMax > 0 && zMax <= 16 : "addOreSingle: The Maximum Z must be greater than 0 and no more than 16";
		assert yMax < 256 && yMax > 0 : "addOreSingle: The Maximum Y must be less than 256 but greater than 0";
		
		WorldGenSingleMinable gen = new WorldGenSingleMinable(block);
		for (int i=0; i<chancesToSpawn; i++)
		{
			int posX = x + random.nextInt(xMax);
			int posZ = z + random.nextInt(zMax);
			int posY = yMin + random.nextInt(yMax - yMin);
			BlockPos pos = new BlockPos(posX, posY, posZ);
			gen.generate(world, random, pos);
		}
	}
	
	private void addOreVein(IBlockState block, World world, Random random, int x, int z, int xMax, int zMax, int maxVeinSize, int chancesToSpawn, int yMin, int yMax)
	{
		assert yMax > yMin : "addOreVein: The Maximum Y must be greater than the minimum Y";
		assert yMin > 0 : "addOreVein: The minimum Y must be greater than 0";
		assert xMax > 0 && xMax <= 16 : "addOreVein: The Maximum X must be greater than 0 and no more than 16";
		assert zMax > 0 && zMax <= 16 : "addOreVein: The Maximum Z must be greater than 0 and no more than 16";
		assert yMax < 256 && yMax > 0 : "addOreVein: The Maximum Y must be less than 256 but greater than 0";
		
		WorldGenMinable gen = new WorldGenMinable(block, maxVeinSize);
		
		for (int i=0; i<chancesToSpawn; i++)
		{
			int posX = x + random.nextInt(xMax);
			int posZ = z + random.nextInt(zMax);
			int posY = yMin + random.nextInt(yMax - yMin);
			gen.generate(world, random, new BlockPos(posX, posY, posZ));
		}
	}
	
	class LayerData
	{
		int topLevel;	// true height in world tiles
		EnumRockType rocktype;
		
		LayerData(int top, EnumRockType rocktype)
		{
			this.topLevel = top;
			this.rocktype = rocktype;
		}
	}
}
