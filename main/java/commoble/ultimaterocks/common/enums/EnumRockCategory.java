package commoble.ultimaterocks.common.enums;

public enum EnumRockCategory
{
	SEDIMENTARY,
	PLUTONIC,
	VOLCANIC,
	METAMORPHIC
}
