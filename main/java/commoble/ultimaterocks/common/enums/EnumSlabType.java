package commoble.ultimaterocks.common.enums;

public enum EnumSlabType
{
	STONE_SLAB(0, "stone_slab"),
	COBBLESTONE_SLAB(1, "cobblestone_slab"),
	BRICK_SLAB(2, "stone_brick_slab");
	
    private static final EnumSlabType[] ID_LOOKUP = new EnumSlabType[values().length];
    
    private final int id;
    private final String name;
    
    private EnumSlabType(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getID()
    {
        return this.id;
    }


    /**
     * Calling type.getName() returns the defined name, e.g. cobblestone
     * calling type.name returns the system name, e.g. COBBLESTONE
     */
    public String getName()
    {
        return this.name;
    }

    public String toString()
    {
        return this.name;
    }

    public static EnumSlabType byID(int id)
    {
        if (id < 0 || id >= ID_LOOKUP.length)
        {
            id = 0;
        }

        return ID_LOOKUP[id];
    }

    static
    {
        for (EnumSlabType blockType : values())
        {
            ID_LOOKUP[blockType.getID()] = blockType;
        }
    }
}
