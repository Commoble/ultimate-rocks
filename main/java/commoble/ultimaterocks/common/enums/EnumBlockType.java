package commoble.ultimaterocks.common.enums;

public enum EnumBlockType
{
	COBBLESTONE(0, "cobblestone"),
	MOSSY_COBBLESTONE(1, "mossy_cobblestone"),
	STONE_BRICK(2, "stone_bricks"),
	CRACKED_BRICK(3, "cracked_stone_bricks"),
	MOSSY_STONE_BRICK(4, "mossy_stone_bricks");
	
    private static final EnumBlockType[] ID_LOOKUP = new EnumBlockType[values().length];
    
    private final int id;
    private final String name;
    
    private EnumBlockType(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getID()
    {
        return this.id;
    }


    /**
     * Calling type.getName() returns the defined name, e.g. cobblestone
     * calling type.name returns the system name, e.g. COBBLESTONE
     */
    public String getName()
    {
        return this.name;
    }

    public String toString()
    {
        return this.name;
    }

    public static EnumBlockType byID(int id)
    {
        if (id < 0 || id >= ID_LOOKUP.length)
        {
            id = 0;
        }

        return ID_LOOKUP[id];
    }

    static
    {
        for (EnumBlockType blockType : values())
        {
            ID_LOOKUP[blockType.getID()] = blockType;
        }
    }
}
