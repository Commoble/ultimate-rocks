package commoble.ultimaterocks.common.enums;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.text.TextFormatting;

public enum EnumRockType implements IStringSerializable
{
    DIORITE(0, "diorite", EnumRockCategory.PLUTONIC),
    GABBRO(1, "gabbro", EnumRockCategory.PLUTONIC),
    GRANITE(2, "granite", EnumRockCategory.PLUTONIC),
    ANDESITE(3, "andesite", EnumRockCategory.VOLCANIC),
    BASALT(4, "basalt", EnumRockCategory.VOLCANIC),
    RHYOLITE(5, "rhyolite", EnumRockCategory.VOLCANIC),
    GNEISS(6, "gneiss", EnumRockCategory.METAMORPHIC),
    MARBLE(7, "marble", EnumRockCategory.METAMORPHIC),
    PHYLLITE(8, "phyllite", EnumRockCategory.METAMORPHIC),
    SCHIST(9, "schist", EnumRockCategory.METAMORPHIC),
    SLATE(10, "slate", EnumRockCategory.METAMORPHIC),
    CHERT(11, "chert", EnumRockCategory.SEDIMENTARY),
    DOLOMITE(12, "dolomite", EnumRockCategory.SEDIMENTARY),
    HALITE(13, "halite", EnumRockCategory.SEDIMENTARY),
    LIMESTONE(14, "limestone", EnumRockCategory.SEDIMENTARY),
    SHALE(15, "shale", EnumRockCategory.SEDIMENTARY);

    private static final EnumRockType[] ID_LOOKUP = new EnumRockType[values().length];
    
    private final int rockID;
    private final String name;
    private final EnumRockCategory cat;
    
    private EnumRockType(int id, String name, EnumRockCategory cat)
    {
        this.rockID = id;
        this.name = name;
        this.cat = cat;
    }

    public int getRockID()
    {
        return this.rockID;
    }
    
    public EnumRockCategory getCategory()
    {
    	return this.cat;
    }


    /**
     * Calling type.getName() returns the defined name, e.g. gneiss
     * calling type.name returns the system name, e.g. GNEISS
     */
    public String getName()
    {
        return this.name;
    }

    public String toString()
    {
        return this.name;
    }

    public static EnumRockType byRockID(int id)
    {
        if (id < 0 || id >= ID_LOOKUP.length)
        {
            id = 0;
        }

        return ID_LOOKUP[id];
    }

    static
    {
        for (EnumRockType rockType : values())
        {
            ID_LOOKUP[rockType.getRockID()] = rockType;
        }
    }
}