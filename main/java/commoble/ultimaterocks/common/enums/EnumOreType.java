package commoble.ultimaterocks.common.enums;

public enum EnumOreType
{
	STONE(0, "stone"),
	COAL(1, "coal"),
	IRON(2, "iron"),
	REDSTONE(3, "redstone"),
	GOLD(4, "gold"),
	DIAMOND(5, "diamond"),
	LAPIS(6, "lapis"),
	EMERALD(7, "emerald");

    private static final EnumOreType[] ID_LOOKUP = new EnumOreType[values().length];
    private final int oreID;
    private final String name;
    private EnumOreType(int id, String name)
    {
        this.oreID = id;
        this.name = name;
    }

    public int getOreID()
    {
        return this.oreID;
    }


    /**
     * Calling type.getName() returns the defined name, e.g. gneiss
     * calling type.name returns the system name, e.g. GNEISS
     */
    public String getName()
    {
        return this.name;
    }

    public String toString()
    {
        return this.name;
    }

    public static EnumOreType byOreID(int id)
    {
        if (id < 0 || id >= ID_LOOKUP.length)
        {
            id = 0;
        }

        return ID_LOOKUP[id];
    }

    static
    {
        for (EnumOreType oreType : values())
        {
            ID_LOOKUP[oreType.getOreID()] = oreType;
        }
    }
}
