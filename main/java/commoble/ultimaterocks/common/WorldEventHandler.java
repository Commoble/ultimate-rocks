package commoble.ultimaterocks.common;

import commoble.ultimaterocks.common.world.GeoTables;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Event handler for World events
 *
 */
@Mod.EventBusSubscriber
public class WorldEventHandler
{
	// this seems to be a good event to use to guarantee running before the world tries to generate
	@SubscribeEvent(priority=EventPriority.HIGHEST)
	public static void onCreateSpawnPosition(WorldEvent.CreateSpawnPosition event)
	{	// TODO only do this for vanilla overworld for now
		if (!event.getWorld().isRemote && event.getWorld().provider.getDimension()==0)
		{
			// code in this block only runs on the server thread
			
			// have the world generator generate perlin tables
			CommonProxy.worldGenManager.genNoiseTables(event);
			GeoTables.generateTables(event);
		}
	}
}
