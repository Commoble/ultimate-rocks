package commoble.ultimaterocks.common;

import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = UltimateRocksMod.MODID, version = UltimateRocksMod.VERSION, name=UltimateRocksMod.NAME)
public class UltimateRocksMod
{
	@Instance("ultimaterocks")	// the static instance of the mod class
	public static UltimateRocksMod instance = new UltimateRocksMod();
	
    public static final String MODID = "ultimaterocks";
    public static final String VERSION = "1.0";
    public static final String NAME="Ultimate Rocks";
    
    @SidedProxy(clientSide="commoble.ultimaterocks.client.CombinedClientProxy",
    		serverSide = "commoble.ultimaterocks.server.DedicatedServerProxy")
    public static CommonProxy proxy;
    
    /**
     * Run before anything else; read the config, create blocks, items, etc, register w/ GameRegistry
     */
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	proxy.preInit(event);
    }
    
    /**
     * Setup anything that doesn't go in pre- or post-init. Build data structures, register recipes,
     * send FMLInterModComms messages to other mods
     */
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        // some example code
        proxy.load(event);
    }
    
    /**
     * Handle interaction with other mods, complete setup base on this
     */
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	proxy.postInit(event);
        //System.out.println("DIRT BLOCK >> "+Blocks.DIRT.getUnlocalizedName());
    }
}