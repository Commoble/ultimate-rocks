package commoble.ultimaterocks.common;

import commoble.ultimaterocks.common.blocks.BlockOreVariant;
import commoble.ultimaterocks.common.blocks.BlockRedstoneVariant;
import commoble.ultimaterocks.common.blocks.BlockStoneVariant;
import commoble.ultimaterocks.common.items.ItemBlockVariants;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Event handler for registering Blocks, Enchantments, Items, Potions, SoundEvents, and Biomes
 * @author Joseph
 *
 */
@Mod.EventBusSubscriber
public class RegistryEventHandler
{
	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> registry = event.getRegistry();
		CombinationTables.registerAllBlocks(registry);
		
		//CommonProxy.blockDiorite = registerBlock(registry, new BlockStoneVariants(), "diorite");
		//CommonProxy.blockGabbro = registerBlock(registry, new BlockStoneVariants(), "gabbro");
		//CommonProxy.blockRhyolite = registerBlock(registry, new BlockStoneVariants(), "rhyolite");
		//CommonProxy.blockGneiss = registerBlock(registry, new BlockStoneVariants(), "gneiss");
		//CommonProxy.blockLimestone = registerBlock(registry, new BlockStoneVariants(), "limestone");
		
		// register a variant for each rock type
		/*for (int i=0; i<CommonProxy.ROCK_TYPE_COUNT; i++)
		{
			CommonProxy.blockStoneVariants[i] =
					registerBlock(registry, new BlockStoneVariant(), BlockStoneVariant.name_list[i]);
			CommonProxy.blockRedstoneVariants[i] =
					registerBlock(registry, new BlockRedstoneVariant(false), BlockRedstoneVariant.name_list[i]);
			CommonProxy.blockRedstoneVariants_lit[i] =
					registerBlock(registry, new BlockRedstoneVariant(true), BlockRedstoneVariant.name_list[i] + "_lit");
			BlockRedstoneVariant.litToUnlitBlockMap.put(CommonProxy.blockRedstoneVariants_lit[i], CommonProxy.blockRedstoneVariants[i]);
			BlockRedstoneVariant.unlitToLitBlockMap.put(CommonProxy.blockRedstoneVariants[i], CommonProxy.blockRedstoneVariants_lit[i]);
			CommonProxy.blockCoalVariants[i] =
					registerBlock(registry, new BlockOreVariant(Items.COAL, 0,2), BlockOreVariant.coal_name_list[i]);
			CommonProxy.blockIronVariants[i] =
					registerBlock(registry, new BlockOreVariant(Item.getItemFromBlock(Blocks.IRON_ORE), 0,0), BlockOreVariant.iron_name_list[i]);
			CommonProxy.blockGoldVariants[i] =
					registerBlock(registry, new BlockOreVariant(Item.getItemFromBlock(Blocks.GOLD_ORE), 0,0), BlockOreVariant.gold_name_list[i]);
			CommonProxy.blockDiamondVariants[i] =
					registerBlock(registry, new BlockOreVariant(Items.DIAMOND, 3,7), BlockOreVariant.diamond_name_list[i]);
			CommonProxy.blockLapisVariants[i] =
					registerBlock(registry, new BlockOreVariant(Items.DYE, 2,5), BlockOreVariant.lapis_name_list[i]);
			CommonProxy.blockEmeraldVariants[i] =
					registerBlock(registry, new BlockOreVariant(Items.EMERALD, 3,7), BlockOreVariant.emerald_name_list[i]);
		}*/
		
		/*CommonProxy.blockStoneVariants = registerBlock(registry, new BlockStoneVariants(), "rocks_stone");
		CommonProxy.blockRedstoneVariants = registerBlock(registry, new BlockRedstoneVariants(false), "ore_redstone");
		CommonProxy.blockRedstoneVariants_lit = (BlockRedstoneVariants)registerBlock(registry, new BlockRedstoneVariants(true), "ore_redstone_lit").setUnlocalizedName("ore_redstone");
		*/
	}
	
	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> registry = event.getRegistry();
		CombinationTables.registerAllItems(registry);
		
		/*CommonProxy.itemBlockDiorite = registerItemBlock(registry, new ItemBlock(CommonProxy.blockGabbro), "diorite");
		CommonProxy.itemBlockGabbro = registerItemBlock(registry, new ItemBlock(CommonProxy.blockGabbro), "gabbro");
		CommonProxy.itemBlockRhyolite = registerItemBlock(registry, new ItemBlock(CommonProxy.blockGabbro), "rhyolite");
		CommonProxy.itemBlockGneiss = registerItemBlock(registry, new ItemBlock(CommonProxy.blockGabbro), "gneiss");
		CommonProxy.itemBlockLimestone = registerItemBlock(registry, new ItemBlock(CommonProxy.blockGabbro), "limestone");*/
		
		// register a variant for each rock type
		/*for (int i=0; i<CommonProxy.ROCK_TYPE_COUNT; i++)
		{
			CommonProxy.itemBlockStoneVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockStoneVariants[i]), BlockStoneVariant.name_list[i]);
			CommonProxy.itemBlockRedstoneVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockRedstoneVariants[i]), BlockRedstoneVariant.name_list[i]);
			CommonProxy.itemBlockCoalVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockCoalVariants[i]), BlockOreVariant.coal_name_list[i]);
			CommonProxy.itemBlockIronVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockIronVariants[i]), BlockOreVariant.iron_name_list[i]);
			CommonProxy.itemBlockGoldVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockGoldVariants[i]), BlockOreVariant.gold_name_list[i]);
			CommonProxy.itemBlockDiamondVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockDiamondVariants[i]), BlockOreVariant.diamond_name_list[i]);
			CommonProxy.itemBlockLapisVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockLapisVariants[i]), BlockOreVariant.lapis_name_list[i]);
			CommonProxy.itemBlockEmeraldVariants[i] =
					registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockEmeraldVariants[i]), BlockOreVariant.emerald_name_list[i]);
		}*/
		
		/*CommonProxy.itemBlockStoneVariants = registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockStoneVariants), "rocks_stone");
		CommonProxy.itemBlockRedstoneVariants = registerItemBlock(registry, new ItemBlockVariants(CommonProxy.blockRedstoneVariants), "ore_redstone");
		*/
	}
}
