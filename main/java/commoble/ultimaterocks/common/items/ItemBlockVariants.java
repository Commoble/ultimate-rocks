package commoble.ultimaterocks.common.items;

import commoble.ultimaterocks.common.enums.EnumRockType;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockVariants extends ItemBlock
{
	/**
	 * @param block must implement IRockVariant
	 */
	public ItemBlockVariants(Block block)
	{
		super(block);

		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}
	
	public int getMetadata(int damage)
	{
		return damage;
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return super.getUnlocalizedName(stack) + "#" + (EnumRockType.byRockID(stack.getItemDamage()).getName());
	}
}
