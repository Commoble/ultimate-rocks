package commoble.ultimaterocks.server;

import commoble.ultimaterocks.common.CommonProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * DedicatedServerProxy is used to set up the mod and start it running when installed on a dedicated server.
 * It should not contain (or refer to) any client-side code (e.g. rendering, textures)
 * @author Joseph
 *
 */
public class DedicatedServerProxy extends CommonProxy
{
	@Override
	public void preInit(FMLPreInitializationEvent event)
	{
		super.preInit(event);
	}
	
	@Override
	public void load(FMLInitializationEvent event)
	{
		super.load(event);
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event)
	{
		super.postInit(event);
	}
}
