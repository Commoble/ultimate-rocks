package commoble.ultimaterocks.client;

import java.util.Collection;

import com.google.common.collect.ImmutableList;

import commoble.ultimaterocks.common.CombinationTables;
import commoble.ultimaterocks.common.CommonProxy;
import commoble.ultimaterocks.common.blocks.BlockRedstoneVariant;
import commoble.ultimaterocks.common.blocks.BlockStoneVariant;
import commoble.ultimaterocks.common.enums.EnumRockType;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
* CombinedClient is used to set up the mod and start it running when installed on a normal minecraft client.
* It should not contain any code necessary for proper operation on a DedicatedServer.
* Code required for both normal minecraft client and dedicated server should go into CommonProxy.
* 
* All client-side-specific things (rendering and textures, mostly) goes in here
* 
*/
public class CombinedClientProxy extends CommonProxy
{
	@Override
	public void preInit(FMLPreInitializationEvent event)
	{
		super.preInit(event);	// run CommonProxy's preInit first to get things registered
		
		//ModelBakery.registerItemVariants(CommonProxy.itemBlockStoneVariants, stone_itemblock_model_locations);
		//ModelBakery.registerItemVariants(CommonProxy.itemBlockStoneVariants, redstone_itemblock_model_locations);
		
		// entity renderers
		
		// tile entity renderers
	}
	
	@Override
	public void load(FMLInitializationEvent event)
	{
		super.load(event);
		
		ItemModelMesher mesher = Minecraft.getMinecraft().getRenderItem().getItemModelMesher();
		// register item renderers here -- the mesher hasn't been initialized yet in preinit
		
		for (int i=0; i<CombinationTables.ROCK_TYPE_COUNT; i++)
		{
			// stone and ore blocks
			for (int j=0; j<CombinationTables.ORE_TYPE_COUNT; j++)
			{
				this.registerItemRenderer(mesher, CombinationTables.stoneItemBlockArray[i][j]);
			}
			// building blocks
			for (int j=0; j<CombinationTables.BLOCK_TYPE_COUNT; j++)
			{
				this.registerItemRenderer(mesher, CombinationTables.buildingBlockItemBlocks[i][j]);
			}
			// stairs
			for (int j=0; j<CombinationTables.BLOCK_TYPE_COUNT; j++)
			{
				this.registerItemRenderer(mesher, CombinationTables.cobbleStairItemBlocks[i]);
				this.registerItemRenderer(mesher, CombinationTables.brickStairItemBlocks[i]);
			}
		}
	}
	
	private void registerItemRenderer(ItemModelMesher mesher, Item item)
	{
		mesher.register(item, 0, new ModelResourceLocation(item.getRegistryName().toString(), "inventory"));
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event)
	{
		super.postInit(event);
	}
}
